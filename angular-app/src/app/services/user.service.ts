import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from "rxjs";
import * as UserUrls from '../utils/urls';
import {User} from '../utils/models/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUsers() : Observable<User[]> {
    return this.http.get<User[]>(UserUrls.allUsers);
  }

  me() : Observable<User> {
    return this.http.get<User>(UserUrls.me);
  }

}
