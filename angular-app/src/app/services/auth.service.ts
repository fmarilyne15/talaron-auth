import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from "rxjs";
import { AuthToken } from '../utils/models/AuthToken';
import { Router } from '@angular/router';

/**
 * services pour: s'authentifier, se déconnecter, rafraîchissement du jeton
 * @author Marilyne Ferreira
 */
@Injectable()
export class AuthService {

  constructor(private http: HttpClient, public router: Router) { }

  /**
   * Envoyer la requête pour le jeton d'authentification 
   * @param loginPayload login et mot de passe
   */
  login(loginPayload) : Observable<AuthToken>{
    
   let oauth2_token_endpoint = 'http://localhost:8081/oauth/token';
   let oauth2_client_id = 'advcomp-client';
   let oauth2_client_secret = 'secret';

    const httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + btoa(oauth2_client_id + ':' + oauth2_client_secret)
        })
    };

    const body = 'client_id={0}&client_secret={1}&grant_type=password&username={2}&password={3}'
        .replace('{0}', oauth2_client_id)
        .replace('{1}', oauth2_client_secret)
        .replace('{2}', loginPayload.username)
        .replace('{3}', loginPayload.password);
    return this.http.post<AuthToken>(oauth2_token_endpoint, body, httpOptions);
  }

  /**
   * Envoyer la requête pour le jeton de rafraîchissement  
   */
  refreshToken() : Observable<AuthToken>{
    
    let oauth2_token_endpoint = 'http://localhost:8081/oauth/token';
    let oauth2_client_id = 'advcomp-client';
    let oauth2_client_secret = 'secret';
 
     const httpOptions = {
         headers: new HttpHeaders({
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': 'Basic ' + btoa(oauth2_client_id + ':' + oauth2_client_secret)
         })
     };
 
     let token = JSON.parse(window.localStorage.getItem('token'));
     const body = 'client_id={0}&client_secret={1}&grant_type=refresh_token&refresh_token={2}'
         .replace('{0}', oauth2_client_id)
         .replace('{1}', oauth2_client_secret)
         .replace('{2}', token.refresh_token)
     return this.http.post<AuthToken>(oauth2_token_endpoint, body, httpOptions);
   }
 
   /**
    * si l'utilisateur n'est pas authentifié, renvoi à la page login
    * @returns vrai si l'utilisateur est authentifié, faux sinon
    */
  isLogged() : boolean {
    let token = window.localStorage.getItem('token');
    if (null == token){
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

  /**
   * supprime le jeton de la mémoire, renvoi à la page login
   */
  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }

  /**
   * vérifie si l'utilisateur a au moins un privilège de la liste 
   * @param privileges liste des privilèges à vérifier 
   * @returns vrai si l'utilisateur a au moins un privilège de la liste passé en paramètre, faux sinon
   */
  hasAnyPrivileges(privileges: String[]) {
    let token = JSON.parse(window.localStorage.getItem('token'));
    let userPrivilege : String[] = token.privileges;
    for(let p of privileges){
      if(userPrivilege.includes(p)){
        return true;
      }
    }
    return false;
  }
}