import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Injectable} from "@angular/core";

/**
 * services pour: intercepte chaque requête, pour injecter le jeton d'authentification sauf 
 * pour la requête d'authentification et de rafraîchissement du jeton 
 * @author Marilyne Ferreira
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = JSON.parse(window.localStorage.getItem('token'));
    let is_token_refresh = JSON.parse(window.localStorage.getItem('is_token_refresh'));
    if (null != token && null == is_token_refresh) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + token.access_token
        }
      });
    console.log("in intercept  " )
    }
    return next.handle(request);
  }
}