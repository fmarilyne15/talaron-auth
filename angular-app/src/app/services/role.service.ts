import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs";
import * as RoleUrls from '../utils/urls';
import { Role } from '../utils/models/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private http: HttpClient) { }

  getRoles() : Observable<Role[]> {
    return this.http.get<Role[]>(RoleUrls.allRoles);
  }

  registerRole(Role: Role): Observable<Role> {
    return this.http.post<Role>(RoleUrls.registerRole, Role);
  }

}
