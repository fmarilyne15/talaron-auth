import { Component, OnInit } from '@angular/core';
import { Role } from '../utils/models/role';
import { Router } from '@angular/router';
import {RoleService} from "../services/role.service";

@Component({
  selector: 'app-list-role',
  templateUrl: './list-role.component.html',
  styleUrls: ['./list-role.component.css']
})
export class ListRoleComponent implements OnInit {

  roles: Role[];

  constructor(private router: Router, private roleService: RoleService) { }

  ngOnInit() {
    this.roleService.getRoles()
      .subscribe( data => {
          this.roles = data;
          console.log(this.roles)
      });
  }
}
