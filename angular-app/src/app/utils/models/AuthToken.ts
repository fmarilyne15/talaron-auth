export interface AuthToken {
    access_token: string,
    token_type: string,
    expires_in: string,
    refresh_token: string,
    scope: Array<string>
}