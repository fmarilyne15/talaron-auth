import { Role } from './role';

export class User {

    id: number;
    lastName: string;
    firstName: string;
    username: string;
    email: string;
    roles: Role[];
    password: string;

  }