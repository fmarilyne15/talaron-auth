export const baseUrl = 'http://localhost:8081/api/';

/************ Right controller backend baseUrls **************/
export const allRoles = `${baseUrl}role/`;
export const registerRole = `${baseUrl}role/register`;
export const updateRole = `${baseUrl}role/update/`;
export const deleteRole = `${baseUrl}role/delete/`;


/************ User controller backend baseUrls **************/
export const allUsers = `${baseUrl}user/`;
export const registerUser = `${baseUrl}user/register/`;
export const updateUser = `${baseUrl}user/update/`;

export const me = `${baseUrl}me/`;