import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';
import { AuthService } from '../services/auth.service';

/**
 * directive qui permet de faire un ngif sur un composant
 * @author Marilyne Ferreira
 */
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[hasPrivilege]'
})
export class HasPrivilegeDirective {
  private privileges: String[] = [];

  @Input()
  set hasPrivilege(p) {
    this.privileges = p;
    this.updateView();
  }

  constructor(private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef, private authService: AuthService) {}

  private updateView() {
    if (this.authService.hasAnyPrivileges(this.privileges)) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }
}
