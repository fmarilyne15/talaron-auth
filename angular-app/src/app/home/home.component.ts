import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private interval;

  constructor(private authService: AuthService, private cookieService: CookieService) { }

  ngOnInit() {
    this.refresh_token_timer();
  }

  /**
   * Tant qu'un utilisateur est connecté, on lance 
   * la requête du rafraîchissement tous les t minutes 
   * (où t est le temps de validité du token moins 10 secondes)
   * La validité se dans un cookie
   */
  refresh_token_timer(){
    let validity = parseInt(this.cookieService.get('validity'));
    this.interval = setInterval(() => {
      if(!this.authService.isLogged()){
        // sinon le timer continue après être déconnecté 
        clearInterval(this.interval);
        return;
      }
      if (validity >10){
        validity -= 10;
      } 
      window.localStorage.setItem('is_token_refresh', 'true');
      this.authService.refreshToken().subscribe(
        response => {
          window.localStorage.setItem('token', JSON.stringify(response));
          this.cookieService.set("validity", response.expires_in);
        console.log("response refresh_token validity " + this.cookieService.get('validity'));
        },
        error => {
            console.log(error);
        });
        localStorage.removeItem('is_token_refresh');
  
  }, validity * 1000);

  }
}
