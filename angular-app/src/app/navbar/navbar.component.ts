import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(    private auth: AuthService, public router: Router) {}
  
  ngOnInit() {
  }

  logout() {
    this.auth.logout();
  }

  roles(){
    this.router.navigate(['list-role']);
  }

  users(){
    this.router.navigate(['list-user']);
  }
}
