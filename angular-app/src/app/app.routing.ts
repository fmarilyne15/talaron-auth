import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {ListUserComponent} from "./list-user/list-user.component";
import { ListRoleComponent } from './list-role/list-role.component';
import { AuthGuard } from './guards/auth.guard.';
import { PrivilegeGuard } from './guards/privilege.guard';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path : '', canActivate : [AuthGuard], component : HomeComponent,
    children: [
      { path: 'list-user', 
        canActivate : [PrivilegeGuard],
        data : {privileges : ['can_see_list_user']},
        component: ListUserComponent },
      { path: 'list-role', 
      canActivate : [PrivilegeGuard],
      data : {privileges : ['can_see_list_role']},
      component: ListRoleComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  {path : '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(routes);