import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
/**
 * guard pour vérifier si l'utilisateur est authentifié 
 * @author Marilyne Ferreira
 */
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {}
  canActivate(): boolean {
    if (!this.auth.isLogged){
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}