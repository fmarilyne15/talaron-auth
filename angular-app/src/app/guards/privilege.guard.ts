import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';
/**
 * guard pour vérifier si l'utilisateur a certains privilèges 
 * @author Marilyne Ferreira
 */
@Injectable({
  providedIn: 'root'
})
export class PrivilegeGuard implements CanActivate  {
  
  constructor(private authService: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const privileges = route.data['privileges'] as Array<string>;
    if (privileges) {
      return this.authService.hasAnyPrivileges(privileges);
    } 
    else { 
      return true; 
    }
  }

}
