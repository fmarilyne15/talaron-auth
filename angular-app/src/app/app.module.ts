import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import { CookieService } from 'ngx-cookie-service';

import {routing} from "./app.routing";
import { AppComponent } from './app.component';;
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ListUserComponent } from './list-user/list-user.component';
import { ListRoleComponent } from './list-role/list-role.component';
import {AuthService} from "./services/auth.service";
import {TokenInterceptor} from "./services/interceptor";
import { HasPrivilegeDirective } from './utils/hasPrivilege.directive';
import { AuthGuard } from './guards/auth.guard.';
import { PrivilegeGuard } from './guards/privilege.guard';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    ListUserComponent,
    ListRoleComponent,
    HasPrivilegeDirective,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    routing,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [CookieService, PrivilegeGuard, AuthGuard, AuthService, {provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi : true}],
  bootstrap: [AppComponent]
})
export class AppModule { }

