import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush

})


  export class AppComponent {
    constructor(private auth: AuthService) {}
    title = 'angular-app';

}