package org.aisl.advcomp.security.rest;

import java.util.List;

import org.aisl.advcomp.security.dao.RoleDao;
import org.aisl.advcomp.security.exception.AlreadyActivateAccountException;
import org.aisl.advcomp.security.exception.BadActivationKeyException;
import org.aisl.advcomp.security.models.User;
import org.aisl.advcomp.security.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * UserController représente les routes pour la gestion des comptes
 * utilisateurs
 * @author Fabien Belugou
 */
@Controller
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    IUserService userService;
    RoleDao roleDao;
    
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("")
    public ResponseEntity<List<User>> getAllUser(){
        return new ResponseEntity<>(userService.getAllUser(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/register")
    public ResponseEntity<User> registerUser(@RequestBody User user) {
        user.grantAuthority(roleDao.findByName("ROLE_USER"));
        User u = userService.registerUser(user);
        return new ResponseEntity<>(u, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/me")
    public ResponseEntity<User> me() {
        try {
            String username =  SecurityContextHolder.getContext().getAuthentication().getName();
            User u = userService.findUserByUsername(username);
            return new ResponseEntity<>(u, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/activate")
    @ResponseBody
    public ResponseEntity<String> activate(@RequestParam("uuid") String username, @RequestParam("k") String key) {
        try {
            userService.activateAccount(username, key);
            return new ResponseEntity<>("Compte activé !", HttpStatus.OK);
        } catch (BadActivationKeyException e) {
            return new ResponseEntity<>("Bad activation key !", HttpStatus.UNAUTHORIZED);
        } catch (AlreadyActivateAccountException e) {
            return new ResponseEntity<>("Already activate account !", HttpStatus.OK);
        }
    }

}