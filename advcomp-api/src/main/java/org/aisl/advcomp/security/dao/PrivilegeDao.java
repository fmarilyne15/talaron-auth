package org.aisl.advcomp.security.dao;

import org.aisl.advcomp.security.models.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Marilyne Ferreira
 */
@Repository
public interface PrivilegeDao extends JpaRepository<Privilege, Long> {
    Privilege findByName(String name);
}