package org.aisl.advcomp.security.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.aisl.advcomp.util.models.DateAudit;

import lombok.Data;

/**
 * Role Model
 * @author Marilyne Ferreira
 */
@Entity
@Data
@Table(name = "roles" , uniqueConstraints = @UniqueConstraint(columnNames = { "name" }))
public class Role extends DateAudit{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
 
    private String name;
 
 /**
   * Privilleges associés au role
   */
  @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "roles_privileges", 
        joinColumns = @JoinColumn(name = "role_id"), 
        inverseJoinColumns = @JoinColumn(name = "privilege_id"))
    private Set<Privilege> privileges = new HashSet<>(); 

    public Role() {}
    public Role(String name){
        this.name = name;
    }

    public void grantPrivilege(Privilege privilege){
        privileges.add(privilege);
    }
}
