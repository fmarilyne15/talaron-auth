package org.aisl.advcomp.security.dao;

import org.aisl.advcomp.security.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Fabien Belugou
 */
@Repository
public interface RoleDao extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
