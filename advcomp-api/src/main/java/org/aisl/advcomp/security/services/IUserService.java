package org.aisl.advcomp.security.services;

import java.util.List;

import org.aisl.advcomp.security.exception.AlreadyActivateAccountException;
import org.aisl.advcomp.security.exception.BadActivationKeyException;
import org.aisl.advcomp.security.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * IUserService
 */
public interface IUserService extends UserDetailsService {
    public User findUserByUsername(String username) throws UsernameNotFoundException;
    public List<User> getAllUser();
    public User registerUser(User user);
    public boolean removeAuthenticatedAccount() throws UsernameNotFoundException;
    public void activateAccount(String username, String key) throws UsernameNotFoundException, BadActivationKeyException, AlreadyActivateAccountException;

}