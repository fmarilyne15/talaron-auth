package org.aisl.advcomp.security.services;

import java.util.List;
import org.aisl.advcomp.security.dao.RoleDao;
import org.aisl.advcomp.security.models.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service pour la gestion des roles
 * @author Marilyne Ferreira
 */
@Service
public class RoleService implements IRoleService {
    @Autowired
    private RoleDao roleDao;
    
    public List<Role> getAllRole() {
        return roleDao.findAll();
    }
    @Transactional 
    public Role registerRole(Role role) {
        
        return roleDao.save(role);
    }
}