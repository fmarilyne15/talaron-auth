package org.aisl.advcomp.security.exception;

/**
 * AlreadyActivateAccountException
 * @author Fabien Belugou
 */
public class AlreadyActivateAccountException extends RuntimeException { }