package org.aisl.advcomp.security.services;

import java.util.List;

import org.aisl.advcomp.security.models.Role;

/**
 * IRoleService
 */
public interface IRoleService  {
    public List<Role> getAllRole();
    public Role registerRole(Role role);
}