package org.aisl.advcomp.security.rest;

import java.util.List;

import org.aisl.advcomp.security.models.Role;
import org.aisl.advcomp.security.services.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * RoleController représente les routes pour la gestion des roles
 * @author Marilyne Ferreira
 */
@Controller
@RequestMapping("/api/role")
public class RoleController {

    @Autowired
    IRoleService roleService;
    @PreAuthorize("hasRole('ROOT')")
    @GetMapping("")
    public ResponseEntity<List<Role>> getAllRole(){
        return new ResponseEntity<>(roleService.getAllRole(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROOT')")
    @PostMapping("/register")
    public ResponseEntity<Role> registerRole(@RequestBody Role role) {
        Role r = roleService.registerRole(role);
        return new ResponseEntity<>(r, HttpStatus.CREATED);
    }

}