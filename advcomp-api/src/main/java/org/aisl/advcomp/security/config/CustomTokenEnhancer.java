package org.aisl.advcomp.security.config;

import java.util.HashMap;
import java.util.Map;

import org.aisl.advcomp.security.models.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

/**
 * Token personnalisé pour ajouter les privilèges aux token
 * 
 * @author Marilyne Ferreira
 */
public class CustomTokenEnhancer implements TokenEnhancer {
 
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>();
        User user = (User) authentication.getPrincipal();
        additionalInfo.put("privileges", user.getPrivilegeUser());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}