package org.aisl.advcomp.security.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.aisl.advcomp.security.dao.RoleDao;
import org.aisl.advcomp.security.dao.UserDao;
import org.aisl.advcomp.security.exception.AlreadyActivateAccountException;
import org.aisl.advcomp.security.exception.BadActivationKeyException;
import org.aisl.advcomp.security.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service pour la gestion des comptes utilisateurs
 * @author Fabien Belugou
 */
@Service
public class UserService implements IUserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> user = userDao.findByUsername( s );
        if ( user.isPresent() ) {
            return user.get();
        } else {
            throw new UsernameNotFoundException(String.format("Username[%s] not found", s));
        }
    }

    public User findUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userDao.findByUsername( username );
        if ( user.isPresent() ) {
            return user.get();
        } else {
            throw new UsernameNotFoundException(String.format("Username[%s] not found", username));
        }
    }
    
    public List<User> getAllUser() {
        return userDao.findAll();
    }

    @Transactional
    public User registerUser(User user) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setActivationKey(UUID.randomUUID().toString());
            System.out.println(user.getActivationKey());
            /*Role role = roleDao.findByName(RoleName.ROLE_USER);
            user.grantAuthority(role);*/
            userDao.save(user);
            // TODO : Send an email with link activation stub
                activateAccount(user.getUsername(), user.getActivationKey());
            return user;
    }

    @Transactional
    public boolean removeAuthenticatedAccount() throws UsernameNotFoundException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = findUserByUsername(username);
        Long del = userDao.deleteUserById(user.getId());
        return del > 0;
    }

    @Transactional
    public void activateAccount(String username, String key) throws UsernameNotFoundException, BadActivationKeyException, AlreadyActivateAccountException {
        User u = findUserByUsername(username);
        if(u.isEnabled()) {
            throw new AlreadyActivateAccountException();
        } else if(u.getActivationKey().equals(key)) {
            u.setAccountNonExpired(true);
            u.setAccountNonLocked(true);
            u.setActivated(true);
            u.setCredentialsNonExpired(true);
            u.setEnabled(true);
        } else {
            throw new BadActivationKeyException();
        }
    }

 
}