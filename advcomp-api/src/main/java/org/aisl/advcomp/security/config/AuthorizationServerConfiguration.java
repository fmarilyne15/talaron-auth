package org.aisl.advcomp.security.config;

import java.util.Arrays;

import org.aisl.advcomp.security.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

/**
 * Serveur d'autorisation
 * @author Fabien Belugou
 */
// Modifié par Marilyne Ferreira - ajouter privilèges au token
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {
    private Logger logger = LoggerFactory.getLogger(AuthorizationServerConfiguration.class);

    @Value("${security.oauth2.accessTokenValiditySeconds}")
    private int accessTokenValiditySeconds;

    @Value("${security.oauth2.refreshTokenValiditySeconds}")
    private int refreshTokenValiditySeconds;

    @Value("${security.jks.classPath}")
    private String classPathJKS;

    @Value("${security.jks.secret}")
    private String secretJKS;
    
    @Value("${security.jks.keyPairName}")
    private String keyPairNameJKS;

    @Value("${newsee.security.secret}")
    private String secretKeyNewSee;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Value("${security.oauth2.resource.id}")
    private String resourceId;
    
    @Autowired
    private AuthenticationManager authenticationManager;

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserService();
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        KeyStoreKeyFactory keyStoreKeyFactory =
                new KeyStoreKeyFactory(
                        new ClassPathResource(classPathJKS),
                        secretJKS.toCharArray());
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair(keyPairNameJKS));
        return converter;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices tokenService = new DefaultTokenServices();
    	tokenService.setTokenStore(tokenStore());
        tokenService.setSupportRefreshToken(true);
        tokenService.setTokenEnhancer(tokenEnhancer());
        return tokenService;
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));
        endpoints
            .userDetailsService(userDetailsService())
            .tokenStore(tokenStore())
            .authenticationManager(authenticationManager)
            .tokenEnhancer(tokenEnhancerChain);
    }
    @Bean
    public TokenEnhancer tokenEnhancer() {
        return new CustomTokenEnhancer();
    }
    
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients
                .inMemory()
                .withClient("advcomp-client")
                    .authorizedGrantTypes("password", "refresh_token")
                    .authorities("ROLE_USER")
                    .scopes("read", "write")
                    .resourceIds(resourceId)            
                    .accessTokenValiditySeconds(accessTokenValiditySeconds)              
                    .refreshTokenValiditySeconds(refreshTokenValiditySeconds)
                    .secret(passwordEncoder.encode(secretKeyNewSee));
    }


}