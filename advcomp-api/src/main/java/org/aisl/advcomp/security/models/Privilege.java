package org.aisl.advcomp.security.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NaturalId;

import lombok.Data;

/**
 * Privilege Model
 * @author Marilyne Ferreira
 */
@Entity
@Data
@Table(name = "privileges", uniqueConstraints = @UniqueConstraint(columnNames = { "name" }))
public class Privilege {
  
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NaturalId
    private String name;
    
    public Privilege(){}
    
    public Privilege(String name){
        this.name = name;
    }
}