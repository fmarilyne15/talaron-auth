package org.aisl.advcomp.security.exception;

/**
 * BadActivationKeyException
 * @author Fabien Belugou
 */
public class BadActivationKeyException extends RuntimeException {
}