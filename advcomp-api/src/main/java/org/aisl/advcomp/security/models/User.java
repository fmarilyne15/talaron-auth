package org.aisl.advcomp.security.models;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.aisl.advcomp.util.models.DateAudit;
import org.hibernate.annotations.NaturalId;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Getter;
import lombok.Setter;

/**
 * User Model représente un compte utilisateur
 * 
 * @author Fabien Belugou
 */
@Entity
@Getter
@Setter
@Table(name = "users", uniqueConstraints = { @UniqueConstraint(columnNames = { "username" }),
    @UniqueConstraint(columnNames = { "email" }) })
public class User extends DateAudit implements UserDetails {

  /**
   * Identifiant unique
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  /**
   * Nom du compte
   */
  @NaturalId
  @NotBlank
  @Size(max = 15)
  private String username;

  /**
   * Password du compte
   */
  @NotBlank
  @JsonProperty(access = Access.WRITE_ONLY)
  @Size(max = 100)
  private String password;

  /**
   * Prenom de l'utilisateur
   */
  @NotBlank
  @Size(max = 50)
  private String firstName;

  /**
   * Nom de l'utilisateur
   */
  @NotBlank
  @Size(max = 50)
  private String lastName;

  /**
   * Email de l'utilisateur
   */
  @NaturalId
  @NotBlank
  @Size(max = 40)
  @Email
  private String email;

  /**
   * Compte Activé ?
   */
  @NotNull
  @Column(nullable = false)
  private boolean activated = false;

  /**
   * Clé d'avtivation du compte
   */
  @Column(name = "activation_key")
  @JsonIgnore
  private String activationKey;

  /**
   * Clé pour réinitialiser le password
   */
  @Size(max = 20)
  @Column(name = "reset_key", length = 20)
  @JsonIgnore
  private String resetKey;

  /**
   * Réinitialisation date d'expiration
   */
  @Column(name = "reset_date")
  private Instant resetDate = null;

  /**
   * Roles associés au compte
   */
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "user_roles", 
    joinColumns = @JoinColumn(name = "user_id"), 
    inverseJoinColumns = @JoinColumn(name = "role_id"))
  private Set<Role> roles = new HashSet<>();

  private boolean accountNonExpired, accountNonLocked, credentialsNonExpired, enabled;

  public User() {
  };

  /**
   * Utilitaire pour créer un compte rapidement
   */
  public User(String firstName, String lastName, String username, String email, String password) {
    this.setFirstName(firstName);
    this.setLastName(lastName);
    this.setUsername(username);
    this.setEmail(email);
    this.setPassword(password);
  }

  public void setUsername(String username) {
    this.username = username.toLowerCase();
  }

  public void setEmail(String email) {
    this.email = email.toLowerCase();
  }

  /**
   * Retourne les roles associés au compte
   */
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    List<GrantedAuthority> authorities = new ArrayList<>();
    roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName().toString())));
    return authorities;
  }

  /**
   * Ajouter un role au compte
   * 
   * @param authority role à ajouter
   */
  public void grantAuthority(Role authority) {
    if (roles == null)
      roles = new HashSet<>();
    roles.add(authority);
  }

  /**
   * Retourner la liste des privilèges en format string d'un utilisateur 
   * @return liste des privilèges
   */
  public Set<String> getPrivilegeUser() {
    Set<String> privileges = new HashSet<>();
    for(Role r : roles){
      r.getPrivileges().forEach(p -> privileges.add(p.getName()));
    }
    return privileges;
  }
  @Override
  public boolean isAccountNonExpired() {
    return accountNonExpired;
  }

  @Override
  public boolean isAccountNonLocked() {
    return accountNonLocked;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return credentialsNonExpired;
  }

  @Override
  public boolean isEnabled() {
    return enabled;
  }
}