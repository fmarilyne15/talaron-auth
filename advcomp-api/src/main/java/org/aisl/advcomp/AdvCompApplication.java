package org.aisl.advcomp;

import javax.transaction.Transactional;

import org.aisl.advcomp.security.dao.RoleDao;
import org.aisl.advcomp.security.dao.PrivilegeDao;
import org.aisl.advcomp.security.models.Privilege;
import org.aisl.advcomp.security.models.Role;
import org.aisl.advcomp.security.models.User;
import org.aisl.advcomp.security.services.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableJpaAuditing
@EnableAsync
public class AdvCompApplication {
  /**
   * Définition de l'implémentation de cryptage des passwords
   */
	@Bean
	public BCryptPasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
  }
  
	public static void main(String[] args) {
		SpringApplication.run(AdvCompApplication.class, args);
	}	

  /**
   * Créations des comptes utilisateurs au démarage :
   *  Admin, User
   *  <ul>
   *  <li> admin => Administrateur de l'application </li
   *  <li> user => Utilisateur </li>
   *  </ul>
   *  <em>pwd => password</em>
   */
	@Bean
	@Transactional
	CommandLineRunner init(
			UserService userService,
			RoleDao roleDao,
			PrivilegeDao privilegeDao
	) {
		return (evt) -> {
			Privilege pListUser = privilegeDao.findByName("can_see_list_user");
			Privilege pRole = privilegeDao.findByName("can_see_list_role");
			Privilege pUser = privilegeDao.findByName("example_user");
			Role RootRole = new Role("ROLE_ROOT");
			RootRole.grantPrivilege(pRole);
			roleDao.save(RootRole);
			Role AdminRole = new Role("ROLE_ADMIN");
			AdminRole.grantPrivilege(pListUser);
			roleDao.save(AdminRole);
			Role UserRole = new Role("ROLE_USER");
			UserRole.grantPrivilege(pUser);
			roleDao.save(UserRole);
			String username = "admin";
			try {
				userService.findUserByUsername(username);
			} catch (UsernameNotFoundException e) {
				//Fake Admin
				User acct = new User();
				acct.setUsername(username);
				acct.setPassword("password");
				acct.setFirstName(username);
				acct.setLastName("LastName");
				acct.setEmail(username + "@2isa.com");
				acct.grantAuthority(UserRole);
				acct.grantAuthority(AdminRole);
				userService.registerUser(acct);

				//Fake User
				acct = new User();
				acct.setUsername("user");
				acct.setPassword("password");
				acct.setFirstName("user");
				acct.setLastName("LastName");
				acct.setEmail("user" + "@2isa.com");
				acct.grantAuthority(UserRole);
				userService.registerUser(acct);

				//Fake Root
				acct = new User();
				acct.setUsername("root");
				acct.setPassword("password");
				acct.setFirstName("root");
				acct.setLastName("LastName");
				acct.setEmail("root" + "@2isa.com");
				acct.grantAuthority(RootRole);
				acct.grantAuthority(UserRole);
				userService.registerUser(acct);


			}
		};
	}
}
