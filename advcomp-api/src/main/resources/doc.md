## Generate Key
>Genérer une paire de clés:
```bash
keytool -genkeypair -alias NewSeeKey -keyalg RSA -keysize 2048 -keypass newsee201866 -keystore newseekeys.jks -storepass newsee201866 -deststoretype pkcs12 -validity 365000
```
