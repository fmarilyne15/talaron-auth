# TokenInterceptor

A chaque requête, on injecte le jeton d'authentification dans l'en-tête. Dans les deux cas suivant, on ne l'injecte pas: 
- Dans la requête de l'authentification puisqu'on demande justement le jeton
- Dans la requête de rafraîchissement du jeton

# Refresh_token (dans home.component.ts)

Dès qu'un utilisateur se connecte, on met un cookie avec la validité du token. L'utilisateur est rédigé automatiquement vers le composant home. Dans ce composant, tant qu'un utilisateur est connecté, on lance la requête du rafraîchissement tous les t minutes (où t est le temps de validité du token moins 10 secondes)